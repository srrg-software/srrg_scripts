# DIRECTORY LAYOUT

OS=`lsb_release -sc`

if [ "${OS}" = "trusty" ]; then
    echo "14.04 selected"
elif [ "${OS}" = "xenial" ]; then
    echo "16.04 selected"
elif [ "${OS}" = "focal" ]; then
    echo "20.04 selected"
else 
    echo "unsuported OS version, you will do the configuration my hand, aborting"
    return 0
fi

SRRG_PACKAGES=${1}
PRIVATE_PACKAGES=("packages_full.txt")

#packages requiring g2o
G2O_CONFIG_PACKAGES=("packages_full.txt" "packages_mapper2d.txt" "packages_navigation_public.txt")

#packages requiring openni2 or freenect
CAMERA_CONFIG_PACKAGES=("packages_full.txt")

MODE=""
if [ -z "$2" ]; then
    echo "No MODE provided, using default one"
       
    MODE="http"
    for item in "${PRIVATE_PACKAGES[@]}"
    do
	if [ ${SRRG_PACKAGES} == "$item" ]; then
	    MODE="git";
	    break
	fi
    done
else
    MODE=${2}
    if [[ "${MODE}" != "http" && "${MODE}" != "git" ]]; then
	echo "unsuported MODE" ${MODE};
	return 0
    fi
fi

echo "CONFIGURATION: "
echo "   OS:       ${OS}"
echo "   MODE:     ${MODE}"
echo "   SRRG_PACKAGES: ${SRRG_PACKAGES}"

sleep 3

CURRENT_DIR=${PWD}
source ${CURRENT_DIR}/config.bash
source ${CURRENT_DIR}/create_directory_layout.bash
source ${CURRENT_DIR}/install_deb_packages.bash
for item in "${CAMERA_CONFIG_PACKAGES[@]}"
do
    if [ ${SRRG_PACKAGES} == "$item" ]; then
	source ${CURRENT_DIR}/install_openni2.bash
	cd ${CURRENT_DIR}
	source ${CURRENT_DIR}/install_freenect.bash
	cd ${CURRENT_DIR}
	break
    fi 
done

# if orazio minimal mode, we don't install g2o
for item in "${G2O_CONFIG_PACKAGES[@]}"
do
    if [ ${SRRG_PACKAGES} == "$item" ]; then
	source ${CURRENT_DIR}/clone_g2o.bash
	cd ${CURRENT_DIR} ;
	break
    fi
done

source ${CURRENT_DIR}/clone_srrg_packages.bash ${MODE} ${SRRG_PACKAGES}
cd ${CURRENT_DIR}
source ${CURRENT_DIR}/init_srrg_workspace.bash
cd ${CURRENT_DIR}
source ${CURRENT_DIR}/build_srrg_workspace.bash
cd ${CURRENT_DIR}
echo "Installation of srrg environment complete."

