source config.bash
PREFIX="";
if [ "${1}" = "git" ]; then
    PREFIX="git@gitlab.com:srrg-software";
else
    PREFIX="https://gitlab.com/srrg-software";
fi;

echo "Mode: ${1}, Prefix: ${PREFIX}"
  
## srrg_packages
CURRENT_DIR=${PWD}
echo "Cloning SRRG packages"
while read in; 
do 
    cd ${SOURCE_SRRG_DIR}
    ARG="${PREFIX}/${in}"
    echo "git clone ${ARG}"
    git clone ${ARG}
    cd ${CURRENT_DIR}
done < ${2}

echo "Done"
