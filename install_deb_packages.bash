source config.bash

OS=`lsb_release -sc`

PACKAGES_16_04="libeigen3-dev 
 libsuitesparse-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 qtdeclarative5-dev \
 qt5-qmake \
 libqglviewer-dev \
 libudev-dev \
 freeglut3-dev \
 arduino \
 arduino-mk"

PACKAGES_14_04="libeigen3-dev \
 libsuitesparse-metis-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 libqt4-dev \
 qt4-qmake \
 libqglviewer-dev \
 libudev-dev \
 freeglut3-dev \
 arduino \
 arduino-mk"

PACKAGES_20_04="ssh \
  openssh-client \
  git \
  build-essential \
  libgtest-dev \
  libglfw3-dev \
  libglu1-mesa-dev \
  emacs \
  unzip \
  libeigen3-dev \
 ros-$ROS_DISTRO-grid-map \
 python3-catkin-tools \
 libsuitesparse-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 libreadline-dev \
 qtdeclarative5-dev \
 qt5-qmake \
 libqglviewer-dev-qt5 \
 libudev-dev \
 freeglut3-dev \
 libgtest-dev \
 arduino \
 arduino-mk \
 python3-pip"



PACKAGES=""
if [ "${OS}" = "trusty" ]; then
    echo "14.04 selected\n"
    PACKAGES=${PACKAGES_14_04};
elif [ "${OS}" = "xenial" ]; then
    echo "16.04 selected\n"
    PACKAGES=${PACKAGES_16_04};
elif [ "${OS}" = "focal" ]; then
    echo "20.04 selected\n"
    PACKAGES=${PACKAGES_20_04};
else
    echo "unknown OS, aborting\n"
    return 0
fi;

echo ${PACKAGES}

# required libraries and packages
echo "Installing ubuntu packages"
sudo apt-get update
sudo apt-get install ${PACKAGES} -y
echo "done"

