source config.bash

#OPENNI2
echo "Downloading and compiling OpenNI2"
cd ${SOURCE_LIBRARIES_DIR}
git clone https://github.com/OpenNI/OpenNI2.git
cd OpenNI2
make ALLOW_WARNINGS=1
echo "Done, if error on pthread ignore"
