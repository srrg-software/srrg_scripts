source config.bash

echo "creating workspace"
# this will be our workspace dir, containing ALL WORKSPACES
WORKSPACES_DIR="${HOME}/workspaces"
mkdir ${WORKSPACES_DIR}

# this will be the dir for our current workspace
WORKSPACE_SRRG_DIR="${WORKSPACES_DIR}/srrg"
mkdir ${WORKSPACE_SRRG_DIR}
cd ${WORKSPACE_SRRG_DIR}
mkdir src
cd ${WORKSPACE_SRRG_DIR}/src
catkin_init_workspace .
cd ..
catkin_make
echo "Done"

echo "making symlinks"
cd ${WORKSPACE_SRRG_DIR}/src
ln -s ${SOURCE_EXTERNAL_DIR}/* .
ln -s ${SOURCE_SRRG_DIR}/* .
echo "Done"

