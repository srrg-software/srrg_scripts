## Directory layout

# here we put all repos
SOURCE_DIR="${HOME}/source"
# here we put all srrg repos               
SOURCE_SRRG_DIR="${SOURCE_DIR}/srrg"
# here we put all external libs      
SOURCE_LIBRARIES_DIR="${SOURCE_DIR}/libraries"
# here we put all external ros packages      
SOURCE_EXTERNAL_DIR="${SOURCE_DIR}/external"

# here we put all workspaces
WORKSPACES_DIR="${HOME}/workspaces"

# here we put the srrg default workspace
WORKSPACE_SRRG_DIR="${WORKSPACES_DIR}/srrg"
