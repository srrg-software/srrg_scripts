# srrg_scripts

This repository contains the Download and Build scripts for the srrg repo(s). 

Fully Supported: 
- Ubuntu 16.04, ROS Kinetic

Partially Supported (in `srrg_boss`: **git checkout 72f24f75** to compile, due to gcc < 4.9 ):
- Ubuntu 14.04, ROS Indigo 

## Prerequisites
one of the two above distributions with the corresponding
ros version installed.
If you use 14.04 and you have Qt5 installed you might have installation
issues.

## Setup

Set-up your ssh keys following the instructions in:

[https://gitlab.com/help/ssh/README.md](https://gitlab.com/help/ssh/README.md)

To install the environment simply do: 

```
source srrg_setup.sh <packages file> [git|http]
```

the `<packages file>` can be either:

* packages_full.txt
* packages_public.txt
* packages_orazio.txt
* packages_mapper2d.txt

depending on what you want/can install. You can also make
your own packages list, as it contains a list
of git packages to download from srrg repos.

Optionally you can provide a second argument to choose the cloning modality `[git|http]`. If this argument is not provided the default modality for the `<packages file>` chosen is used.

Run on a freshly installed machine with ros-desktop-full.

### Maintenance instructions ###

When you add a new package in the srrg environment, please update the following files accordingly:

   * packages_full.txt
   * packages_public.txt