#source config.bash
CURRENT_DIR=`pwd`


bold=$(tput bold)
normal=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
cyan=$(tput setaf 6)

## srrg_packages
echo "Executing $yellow$*$normal on installed ${bold}SRRG packages$normal"

for package in ./*
do
  cd ${CURRENT_DIR}
  if [[ -d "$package" ]]
  then
    echo "Executing git command $yellow$*$normal on  $cyan${package}$normal"
    cd ${package}
    OUT=$(git \
    $* 2>&1)
    if [[ ${OUT} == *"error:"* ]] || [[ ${OUT} == *"fatal:"* ]]; 
    then
      echo "$red${OUT}$normal"
    elif [[ ${OUT} == *"up to date"* ]];
    then
      echo "$green${OUT}$normal"
    else
      echo "${OUT}"
    fi
  fi
done

echo " Done"
cd $CURRENT_DIR
